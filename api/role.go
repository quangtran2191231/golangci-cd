package api

import (
	"golang/internal/model/domain"
	"net/http"

	"github.com/gin-gonic/gin"
)

type getRoleRequest struct {
	ID int64 `uri:"id" binding:"required,min=1"`
}

func (server *Server) GetRoleById(ctx *gin.Context) {

	var req getRoleRequest
	if err := ctx.ShouldBindUri(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, errorResponse(http.StatusBadRequest,"request error " , err.Error()))
		return
	}
	role, err := server.svcRead.GetRoleByIdBiz(ctx,req.ID)
	if err != nil {
		ctx.JSON(http.StatusNotFound, errorResponse(http.StatusNotFound,"get role fail " , err.Error()))
		return
	}
	ctx.JSON(http.StatusOK, role)
}
type  ListRoleParams  struct {
	PageID    int32  `form:"page_id"`
	PageSize   int32  `form:"page_size" `
}

func (server *Server) ListRoles(ctx *gin.Context) {
	var req ListRoleParams
	if err := ctx.ShouldBindUri(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, errorResponse(http.StatusBadRequest,"request error " , err.Error()))
		return
	}
	arg := domain.ListRoleParams{
		Limit:  req.PageSize,
		Offset: (req.PageID - 1) * req.PageSize,
	}
	roles , err := server.svcRead.ListRoleBiz(ctx,arg)
	if err != nil {
		ctx.JSON(http.StatusNotFound, errorResponse(http.StatusNotFound,"List roles fail " , err.Error()))
		return
	}
	ctx.JSON(http.StatusOK, roles)
}