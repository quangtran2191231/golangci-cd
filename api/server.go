package api

import (
	// "golang/internal/model/domain"
	"golang/internal/service"

	"github.com/gin-gonic/gin"
	"github.com/penglongli/gin-metrics/ginmetrics"
	// metric "golang/pkg/metric"
	"go.elastic.co/apm/module/apmgin/v2"
)

type Server struct {
	// store  domain.Store
	router  *gin.Engine
	svcRead service.Service
	svcWrite service.Service
}

func NewServer(svcRead service.Service,svcWrite service.Service ) *Server {

	gin.ForceConsoleColor()
	gin.SetMode(gin.ReleaseMode)
	server := &Server{svcRead: svcRead,svcWrite: svcWrite}
	router := gin.New()
	m := ginmetrics.GetMonitor()

	// +optional set metric path, default /debug/metrics
	m.SetMetricPath("/metrics")
	// +optional set slow time, default 5s
	m.SetSlowTime(10)
	// +optional set request duration, default {0.1, 0.3, 1.2, 5, 10}
	// used to p95, p99
	m.SetDuration([]float64{0.1, 0.3, 1.2, 5, 10})

	// set middleware for gin
	m.Use(router)
	router.Use(apmgin.Middleware(router))
	r := router.Group("/v1")
	
	// http.Handle("/metrics", promhttp.Handler())
	r.POST("/users", server.CreateUser)

	// router.GET("/users", server.ListUser)
	router.GET("/healthy", func(ctx *gin.Context) {
		var str = "application healthy"
		ctx.JSON(200, str)
	})

	r.GET("/role/:id", server.GetRoleById)
	r.GET("/product/:id", server.GetProductById)
	r.GET("/order/:id",server.GetOrderByid)
	r.GET("/role",server.ListRoles)
	server.router = router
	return server
}

func errorResponse(code int, mess string, err string) gin.H {
	return gin.H{"code": code, "mess": mess, "errInfo": err}
}



func (server *Server) Start(address string) error {
	return server.router.Run(address)
}
