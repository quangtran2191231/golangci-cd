package main

import (
	"database/sql"
	"errors"
	"fmt"
	"golang/api"
	"golang/internal/service"

	// "math/rand"
	logrus "github.com/sirupsen/logrus"
	 "go.elastic.co/apm/module/apmsql"
	_ "go.elastic.co/apm/module/apmsql/v2/pq"

	// "golang/internal/model/domain"
	"golang/utils"
	"log"
	"os"
	"time"
)

type Database struct {
	Connection *sql.DB
	err        error
}

var ErrConnectToDatabase = errors.New("cannot Connect to Database")

func init() {

	// Log as JSON instead of the default ASCII formatter.
	logrus.SetFormatter(&logrus.JSONFormatter{})

	// Output to stdout instead of the default stderr
	// Can be any io.Writer, see below for File example
	logrus.SetOutput(os.Stdout)

}

func main() {
	if err := initServer(); err != nil {
		log.Fatal(err)
	}
}

func initServer() error {
	config, err := utils.LoadConfig(".")
	if err != nil {
		fmt.Println("Cannot not load config ", err)
	}
	// config , err  := utils.LoadConfig(".")
	// if err != nil {
	// 	fmt.Println("Cannot not load config ", err)
	// }
	// // println(os.Getenv("DB_SOURCE"))
	// conn , err := sql.Open(config.DBDriver,os.Getenv("DB_SOURCE"))

	// if err != nil {
	// 	fmt.Println("Error when config to db" , err)
	// }
	writeConn := NewWriteConnection()
	readConn := NewReadConnection()

	// newContext, cancel := context.WithTimeout(context.Background(), time.Second*10)
	// defer cancel()


	storeRead := service.NewServiceReadStore(readConn.Connection)
	storeWrite := service.NewServiceWriteStore(writeConn.Connection)
	server := api.NewServer(storeRead,storeWrite)


	err = server.Start(config.ServerAddess)
	if err != nil {
		fmt.Println("cannot start server:", err)
	}
	return nil
}
func NewReadConnection() *Database {
	config, err := utils.LoadConfig(".")
	if err != nil {
		fmt.Println("Cannot not load config ", err)
	}
	// println(os.Getenv("DB_SOURCE"))
	conn, err := apmsql.Open(config.DBDriver, os.Getenv("DB_READ"))
	go func() {
		for  {
			if err := conn.Ping(); err != nil {
				logrus.Errorln("Cannot connect to Database Read")
			}
			time.Sleep(5 * time.Second)
		}
	}()

	return &Database{
		Connection: conn,
		err:        err,
	}
}

func NewWriteConnection() *Database  {
	config, err := utils.LoadConfig(".")
	if err != nil {
		fmt.Println("Cannot not load config ", err)
	}
	// println(os.Getenv("DB_SOURCE"))
	conn, err := apmsql.Open(config.DBDriver, os.Getenv("DB_WRITE"))
	go func() {
		for  {
			if err := conn.Ping(); err != nil {
				logrus.Errorln("Cannot connect to Database Write")
			}
			time.Sleep(5 * time.Second)
		}
	}()

	// logrus.Infoln(conn.Stats().InUse)

	return &Database{
		Connection: conn,
		err:        err,
	}
}