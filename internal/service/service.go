package service

import (
	"context"
	"database/sql"
	"golang/internal/model/domain"
	"golang/utils"

	_ "github.com/lib/pq"
	"github.com/sirupsen/logrus"
	// "log"
)


type ServiceStore struct {
	*domain.Queries
	// WriteQueries *domain.Queries
	dbRead *sql.DB
	dbWrite *sql.DB
}

type Service interface {
	domain.Querier
	RegisterUser(ctx context.Context , param domain.CreateUserParams) (error)
	OrderByIdBiz(ctx context.Context,id int64) (domain.Order,error)
	GetRoleByIdBiz(ctx context.Context,id int64) (domain.Role,error)
	ListRoleBiz(ctx context.Context, arg domain.ListRoleParams) ([]domain.Role,error)
}
func NewServiceReadStore(readConn *sql.DB) Service {
	return &ServiceStore{
		dbRead: readConn,
		Queries: domain.New(readConn),
	}
}
func NewServiceWriteStore(writeConn *sql.DB) Service {
	return &ServiceStore{
		dbRead: writeConn,
		Queries: domain.New(writeConn),
	}
}
func (s *ServiceStore) OrderByIdBiz(ctx context.Context,id int64) (domain.Order,error)  {
	a := domain.NewSQLstore(s.dbRead)
	order , err := a.GetOrdersById(ctx,id)
	if err != nil {
		logrus.Warn("order not exist  ")
		return order,err
	}

	logrus.WithFields(logrus.Fields{
		"orderID": order.ID,
		"createAt":   order.CreatedAt,
	  }).Info("order info ")
	return order,nil
}

func (s *ServiceStore) GetRoleByIdBiz(ctx context.Context,id int64) (domain.Role,error)  {
	a := NewServiceReadStore(s.dbRead)
	role , err := a.GetRole(ctx,id)
	if err != nil {
		logrus.Warn("order not exist  ")
		return role,err
	}
	logrus.WithFields(logrus.Fields{
		"orderID": role.ID,
		"createAt":   role.CreatedAt,
	  }).Info("role info ")

	return role,nil
}
func (s *ServiceStore) ListRoleBiz(ctx context.Context , arg domain.ListRoleParams) ([]domain.Role,error) {
	a := NewServiceWriteStore(s.dbRead)

	// arg := s.
	roles ,err := a.ListRole(ctx,arg)
	if err != nil {
		return nil, err
	}

	return roles , nil
}
// type ParamPasswordAfterHashing struct {
// 	FullName       string `json:"full_name"`
// 	Username       string `json:"username"`
// 	HashedPassword string `json:"hashed_password"`
// 	Email          string `json:"email"`
// 	Mobile         int64  `json:"mobile"`
// 	Roleid         int64  `json:"roleid"`
// }
func (s *ServiceStore) RegisterUser(ctx context.Context , requestParam domain.CreateUserParams)  (error)  {
	req := NewServiceWriteStore(s.dbWrite)
	hashedPassword , err  := utils.HashedPassword(requestParam.HashedPassword) /// hash param request password to hashedpassword
	if err != nil {

		logrus.WithFields(logrus.Fields{
			"Username": requestParam.Username,
			"createAt":   requestParam.FullName,
		  }).Warnf("Error when hashed password   ")
		return err
	}
	// newParam := ParamPasswordAfterHashing{
	// 	FullName: requestParam.FullName,
	// 	Username: requestParam.Username,
	// 	HashedPassword: hashedPassword,
	// 	Email: requestParam.Email,
	// 	Mobile: requestParam.Mobile,
	// 	Roleid: requestParam.Roleid,
	// }
	requestParam.HashedPassword = hashedPassword
	user , err := req.CreateUser(ctx, requestParam)
	if err != nil {
		return err
	}
	logrus.WithFields(logrus.Fields{
		"Username": user.Username,
		"createAt":   user.FullName,
	}).Info("user create  ")
	return  nil
}
